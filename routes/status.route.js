const { Router } = require("express");
const StatusController = require("../controllers/status.controller");
const validatorStatusHandler = require("../middleware/validator.middleware");
const {
  createStatusSchema,
  getStatusSchema,
  updateStatusSchema,
} = require("../schemas/status.schema");
const route = Router();

route.get("/", StatusController.index);
route.get("/create", StatusController.create);
route.post(
  "/create",
  validatorStatusHandler(createStatusSchema, "body"),
  StatusController.store
);
route.get(
  "/edit/:id",
  validatorStatusHandler(getStatusSchema, "params"),
  StatusController.edit
);
route.post(
  "/update/:id/",
  validatorStatusHandler(getStatusSchema, "params"),
  validatorStatusHandler(updateStatusSchema, "body"),
  StatusController.update
);
route.get("/delete/:id", StatusController.destroy);

module.exports = route;
