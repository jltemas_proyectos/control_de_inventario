const { Router } = require("express");
const RoleController = require("../controllers/role.controller");
const validatorStatusHandler = require("../middleware/validator.middleware");
const { createRoleSchema, getRoleSchema, updateRoleSchema } = require("../schemas/role.schema");

const route = Router();

route.get("/", RoleController.index);
route.get("/create", RoleController.create);
route.post("/create", validatorStatusHandler(createRoleSchema, 'body'), RoleController.store);
route.get("/edit/:id", validatorStatusHandler(getRoleSchema, "params"), RoleController.edit);
route.post("/update/:id", validatorStatusHandler(getRoleSchema, "params"), validatorStatusHandler(updateRoleSchema, "body"), RoleController.update);
route.get("/delete/:id", validatorStatusHandler(getRoleSchema, "params"), RoleController.destroy);

module.exports = route;
