const { Router } = require('express');
const PagesController = require("../controllers/pages.controller");

const router = Router();

router.get('/', PagesController.index);
router.get('/login', PagesController.login);
router.get('/register', PagesController.register);
router.post('/register', PagesController.store);
router.get('/dashboard', PagesController.dashboard);

module.exports = router;
