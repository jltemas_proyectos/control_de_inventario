const { Router } = require("express");
const PositionController = require("../controllers/position.controller");
const validatorStatusHandler = require("../middleware/validator.middleware");
const { createPositionSchema, editPositionSchema, getPositionSchema } = require("../schemas/position.schema");

const route = Router();
route.get("/", PositionController.index);
route.get("/create", PositionController.create);
route.post("/create", validatorStatusHandler(createPositionSchema, "body"), PositionController.store);
route.get("/edit/:id", validatorStatusHandler(getPositionSchema, "params"), PositionController.edit);
route.post("/update/:id", validatorStatusHandler(getPositionSchema, "params"), validatorStatusHandler(editPositionSchema, "body"), PositionController.update);
route.get("/delete/:id", validatorStatusHandler(getPositionSchema, "params"), PositionController.destroy);

module.exports = route;
