const { Router } = require('express');
const UserController = require("../controllers/user.controller");


const router = Router();

router.get("/", UserController.index);
router.get("/create", UserController.create);
router.post("/create", UserController.store);
router.get("/edit/:id", UserController.edit);
router.post("/update/:id", UserController.update);
router.get("/delete/:id", UserController.destroy);

module.exports = router;
