var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");

var pagesRouter = require("./routes/index.route");
var usersRouter = require("./routes/users.route");
var statusRouter = require("./routes/status.route");
var rolesRouter = require("./routes/role.route");
var positionRouter = require("./routes/position.route");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Route Aplication
app.use("/", pagesRouter);
app.use("/users", usersRouter);
app.use("/status", statusRouter);
app.use("/roles", rolesRouter);
app.use("/positions",positionRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
