const PositionModel = require("../models/position.model");
const StatusModel = require("../models/status.model");
const Boom = require("@hapi/boom");

class PositionController {
  async index(req, res, next) {
    try {
      const data = {
        title: "Cargos",
        url:"/positions/create",
        buttonName: "Crear Cargos",
        positions: await PositionModel.all(),
      };

      res.status(200).render("modules/position/index", data);
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    try {
      const data = {
        title: "Cargos",
        formName: "Crear Cargos",
        url: "/positions/create",
        method: "POST",
        buttonName: "crear",
        statuses: await StatusModel.all(),
        type: req.path.split("/").join(""),
      };

      res.status(200).render("modules/position/form", data);
    } catch (error) {
      next(error);
    }
  }

  store(req, res, next) {
    try {
      const data = {
        name: req.body.position.trim(),
        description: req.body.description.trim(),
        status_id: req.body.status.trim(),
      };

      PositionModel.create(data);

      res.status(201).redirect("/positions");
    } catch (error) {
      next(error);
    }
  }

  async edit(req, res, next) {
    try {
      const { id } = req.params;
      const position = await PositionModel.findById(id);
      if (position.length == 0) {
        throw Boom.badRequest();
      } else {
        const data = {
          title: "Cargos",
          formName: "Editar Cargos",
          url: `/positions/update/${id}`,
          method: "POST",
          buttonName: "Actualizar",
          statuses: await StatusModel.all(),
          type: req.path.split("/")[1],
          position: position[0],
        };

        res.status(200).render("modules/position/form", data);
      }
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const { id } = req.params;
      const position = await PositionModel.findById(id);
      if (position.length == 0) {
        throw Boom.badRequest();
      } else {
        const data = {
          name: req.body.position.trim(),
          description: req.body.description.trim(),
          status_id: req.body.status.trim(),
        };

        await PositionModel.update(id, data);

        res.status(200).redirect("/positions");
      }
      res.send(position);
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const { id } = req.params;
      const position = await PositionModel.findById(id);
      if (position.length == 0) {
        throw Boom.badRequest();
      } else {
        await PositionModel.delete(id);
        res.status(200).redirect("/positions");
      }
      res.send(position);
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new PositionController();
