const UserModel = require("../models/user.model");

class PagesController {

  index(req, res, next) {
    res.redirect("/login");
  }

  login(req, res, next) {
    res.render('login', { title: 'Inicio De Sessión' });
  }

  register(req, res, next) {
    try {
      res.status(200).render("register");
    } catch (error) {
      next(error);
    }
  }

  async store(req, res, next) {
    try {
      const { acceptTerms } = req.body
      if (acceptTerms === "ok") {
        
        const data = {
          name: req.body.username.trim(),
          email: req.body.email.trim(),
          password: req.body.password.trim(),
          role_id: 1,
          status_id: 1
        }

        await UserModel.create(data);

        res.status(201).redirect("/login");
      } else {
        //...
      }

    } catch (error) {
      next(error);
    }
  }

  dashboard(req, res, next) {
    res.render('dashboard', { title: 'Dashboard' });
  }
}

module.exports = new PagesController();