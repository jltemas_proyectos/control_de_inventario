const RoleModel = require("../models/role.model");
const StatusModule = require("../models/status.model");
const Boom = require("@hapi/boom");

class RoleController {
  async index(req, res, next) {
    try {
     
      const data = {
        title: "Roles",
        url:"/roles/create",
        buttonName: "Crear Role",
        roles: await RoleModel.all(),
      };

      res.status(200).render("modules/roles/index", data);
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    const data = {
      title: "Roles",
      formName: "Crear Roles",
      url: "/roles/create",
      method: "POST",
      buttonName: "crear",
      statuses: await StatusModule.all(),
      type: req.path.split("/").join(""),
    };

    res.status(200).render("modules/roles/form", data);
  }

  async store(req, res, next) {
    try {
      const data = {
        name: req.body.role.trim(),
        description: req.body.description.trim(),
      };

      await RoleModel.create(data);

      res.status(201).redirect("/roles");
    } catch (error) {
      next(error);
    }
  }

  async edit(req, res, next) {
    try {
      const { id } = req.params;
      const role = await RoleModel.findById(id);

      if (role.length == 0) {
        throw Boom.badRequest();
      } else {
        const data = {
          title: "Roles",
          formName: "Editar Roles",
          url: `/roles/update/${id}`,
          method: "POST",
          buttonName: "Actualizar",
          statuses: await StatusModule.all(),
          type: req.path.split("/")[1],
          role: role[0],
        };

        res.status(200).render("modules/roles/form", data);
      }
    } catch (error) {
      next(error);
    }
  }

  update(req, res, next) {
    try {
      const { id } = req.params;

      const data = {
        name: req.body.role.trim(),
        description: req.body.description.trim(),
        status_id: req.body.status,
      };
      
      RoleModel.update(id, data);

      res.redirect("/roles");
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const { id } = req.params;

      const role = await RoleModel.findById(id);

      if (role.length == 0) {
        throw Boom.badRequest();
      } else {
        RoleModel.delete(id);
        res.redirect("/roles");
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new RoleController();
