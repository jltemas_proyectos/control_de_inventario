const UserModel = require("../models/user.model");
const RoleModel = require("../models/role.model");
const StatusModel = require("../models/status.model");
const Boom = require("@hapi/boom");


class UserController {
  async index(req, res, next) {
    try {
      const data = {
        title: "Usuarios",
        url: "/users/create",
        buttonName: "Crear Usuario",
        users: await UserModel.all(),
      };
      res.status(200).render("modules/user/index", data);
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    try {
      const data = {
        title: "Usuarios",
        formName: "Crear Usuarios",
        url: "/users/create",
        method: "POST",
        buttonName: "Crear",
        type: req.path.split("/").join(""),
        statuses: await StatusModel.all(),
        roles: await RoleModel.all()
      }

      res.status(200).render("modules/user/form", data);

    } catch (error) {
      next(error);
    }
  }

  async store(req, res, next) {
    try {
      const data = {
        name: req.body.username,
        email: req.body.email,
        password: req.body.password,
        role_id: req.body.role,
        status_id: req.body.status
      }

      await UserModel.create(data);

      res.status(201).redirect("/users");
    } catch (error) {
      next(error);
    }
  }

  async edit(req, res, next) {
    try {
      const { id } = req.params;

      const user = UserModel.findById(id);

      if (user.length == 0) {
        throw Boom.badRequest();
      } else {
        const data = {
          title: "Usuarios",
          formName: "Editar Usuarios",
          url: `/users/edit/${id}`,
          method: "POST",
          buttonName: "Actualizar",
          type: req.path.split("/")[1],
          statuses: await StatusModel.all(),
          roles: await RoleModel.all()
        }
// res.send(data)
        res.status(200).render("modules/user/form",data);
      }

    } catch (error) {
      next(error);
    }
  }

  update(req, res, next) { }

  destroy(req, res, next) { }
}

module.exports = new UserController()