const StatusModule = require("../models/status.model");
const Boom = require("@hapi/boom");

class StatusController {
  async index(req, res, next) {
    try {
      const data = {
        title: "Estados",
        url:"/status/create",
        buttonName: "Crear Estado",
        statuses: await StatusModule.all(),
      };
      res.status(200).render("modules/status/index", data);
    } catch (error) {
      next(error);
    }
  }

  create(req, res, next) {
    const data = {
      title: "Estados",
      formName: "Crear Estados",
      url: "/status/create",
      method: "POST",
      buttonName: "crear",
      type: req.path.split("/").join(""),
    };

    res.status(200).render("modules/status/form", data);
  }

  async store(req, res, next) {
    try {
      const data = {
        name: req.body.status.trim(),
        description: req.body.description.trim(),
      };

      await StatusModule.create(data);
      res.status(201).redirect("/status");
    } catch (error) {
      next(error);
    }
  }

  async edit(req, res, next) {
    try {
      const { id } = req.params;
      const status = await StatusModule.findById(id);
      if (StatusModule.length == 0) {
        throw Boom.badRequest();
      } else {
        const data = {
          title: "Estados",
          formName: "Editar Estados",
          url: `/status/update/${id}`,
          method: "POST",
          buttonName: "Actualizar",
          type: req.path.split("/")[1],
          status: status[0],
        };
        res.status(200).render(`modules/status/form`, data);
      }
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const { id } = req.params;

      const data = {
        name: req.body.status.trim(),
        description: req.body.description.trim()
      };
      console.log(data);
      StatusModule.update(id, data);
      res.redirect("/status");
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const { id } = req.params;
      await StatusModule.delete(id);
      res.status(201).redirect("/status");
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new StatusController();
