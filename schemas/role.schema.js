const Joi = require('joi');

const id = Joi.number().integer();
const status = Joi.number().integer();
const role = Joi.string().min(5);
const description = Joi.string();

const createRoleSchema = Joi.object({
    role: role.required(),
    status: status.required(),
    description: description.required()
});

const updateRoleSchema = Joi.object({
    role: role.required(),
    status: status.required(),
    description: description.required()
});

const getRoleSchema = Joi.object({
    id: id.required()
});

module.exports = { createRoleSchema, updateRoleSchema, getRoleSchema }