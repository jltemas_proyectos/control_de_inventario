const Joi = require("joi");

const id = Joi.number().integer();
const position = Joi.string().min(4);
const description = Joi.string();
const status = Joi.number().integer();

const getPositionSchema = Joi.object({
  id: id.required(),
});

const createPositionSchema = Joi.object({
  position: position.required(),
  description: description.required(),
  status: status.required(),
});

const editPositionSchema = Joi.object({
  position: position.required(),
  description: description.required(),
  status: status.required(),
});

module.exports = {
  getPositionSchema,
  createPositionSchema,
  editPositionSchema,
};
