const Joi = require("joi");

const id = Joi.number().integer();
const status = Joi.string().min(5);
const description = Joi.string().max(100);

const createStatusSchema = Joi.object({
  status: status.required(),
  description: description.required(),
});

const updateStatusSchema = Joi.object({
  status: status.required(),
  description: description.required(),
});

const getStatusSchema = Joi.object({
  id: id.required(),
});

module.exports = { createStatusSchema, updateStatusSchema, getStatusSchema };
