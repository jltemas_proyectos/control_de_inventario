const Model = require("./model");

class RoleModel extends Model {
  constructor() {
    super("roles");
  }
}

module.exports = new RoleModel();
