const Model = require("../models/model");

class UserModel extends Model {
    constructor(){
        super("users");
    }
}

module.exports = new UserModel();