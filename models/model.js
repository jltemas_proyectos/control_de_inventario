const connection = require("../services/data/connection");

class Model {
  constructor(table) {
    this.table = table;
  }

  /**
   *
   * @returns retorna todos los elementos de la tabla
   */
  all() {
    return new Promise((resolve, reject) => {
      const sql = `SELECT * FROM ${this.table}`;
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  /**
   * retorna un elemento de la tabla por medio de su Id
   * @param {number} id
   * @returns Status
   */
  findById(id) {
    return new Promise((resolve, reject) => {
      const sql = `SELECT * FROM ${this.table} WHERE id=?`;
      connection.query(sql, id, (err, result) => {
        if (err) {
          reject(err);
        }
        resolve(result);
      });
    });
  }

  create(data) {
    return new Promise((resolve, reject) => {
      const sql = `INSERT INTO ${this.table} SET ?`;
      connection.query(sql, data, (err, row) => {
        if (err) {
          reject(err);
        }
        resolve(row);
      });
    });
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      const sql = `DELETE FROM ${this.table} WHERE id= ?`;
      connection.query(sql, id, (err, row) => {
        if (err) {
          reject(err);
        }
        resolve(row);
      });
    });
  }

  update(id, data) {
    return new Promise((resolve, reject) => {
      const sql = `UPDATE ${this.table} SET ? WHERE id=?`;
      connection.query(sql, [data, id], (err, row) => {
        if (err) {
          reject(err);
        }
        resolve(row);
      });
    });
  }
}

module.exports = Model;
