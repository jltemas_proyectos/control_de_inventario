const Model = require('./model');

class PositionModel extends Model {
    constructor(){
        super("positions");
    }
}

module.exports = new PositionModel();