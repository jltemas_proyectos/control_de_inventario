const Model = require('./model')

class StatusModule extends Model {

  constructor(){
    super('status');
  }

}

module.exports = new StatusModule();