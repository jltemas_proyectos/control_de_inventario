function clearData(params) {
  const data = {}
  for (let dato in params) {
    data[dato] = params[dato].trim();
  }
  return data
}

function validatorHandler(schema, property) {
  return async (req, res, next) => {
    try {
      const formData = req[property];
      const data = clearData(formData)
      await schema.validateAsync(data, {
        abortEarly: false, //esta propiedad del objeto permite validad todos los errores a la vez
      });
      next();
    } catch (error) {
      next(error);
    }
  };
}

module.exports = validatorHandler;
