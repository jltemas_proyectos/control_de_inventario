const mysql = require('mysql');
const authentication = require('./config');

const connection = mysql.createConnection(authentication)

connection.connect(function (err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  console.log('connected as id ' + connection.threadId + ' a la Base De Datos');
});

module.exports = connection;

