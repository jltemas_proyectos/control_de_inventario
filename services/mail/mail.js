const nodemailer = require("nodemailer");
const connection = require('./connection');
// console.log(connection);
// async..await is not allowed in global scope, must use a wrapper
async function main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: connection.host,
    port: connection.port,
    secure: false, // true for 465, false for other ports
    auth: {
      user: connection.authentication.user, // generated ethereal user
      pass: connection.authentication.password, // generated ethereal password
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: connection.authentication.user, // sender address
    to: "gerardoa_garzon@coomeva.com.co", // list of receivers
    subject: "Pruena desde Node", // Subject line
    text: "Esto es una pruba de NodeMailer", // plain text body
    html: "<h2>Prueba</h2>", // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal accoun
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error);
